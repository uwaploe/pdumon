package port

import (
	"fmt"
	"io"
	"net"
	"time"

	"go.bug.st/serial"
)

// Port represents a serial or TCP port interface
type Port interface {
	io.ReadWriteCloser
	CanChangeBaud() bool
	SetBaud(baudrate int) error
	EchoMode(state bool)
}

type serialPort struct {
	serial.Port
	device   string
	echoMode bool
}

func openSerialPort(dev string, baud int, timeout time.Duration) (serial.Port, error) {
	mode := &serial.Mode{
		BaudRate: baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(dev, mode)
	if err != nil {
		return port, fmt.Errorf("open %s: %w", dev, err)
	}

	if timeout > 0 {
		port.SetReadTimeout(timeout)
	}

	return port, nil
}

func SerialPort(device string, baud int,
	timeout time.Duration) (*serialPort, error) {
	p, err := openSerialPort(device, baud, timeout)
	if err != nil {
		return nil, err
	}

	return &serialPort{device: device, Port: p}, nil
}

func (s *serialPort) String() string {
	return s.device
}

func (s *serialPort) SetBaud(baud int) error {
	mode := &serial.Mode{
		BaudRate: baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	return s.Port.SetMode(mode)
}

func (s *serialPort) CanChangeBaud() bool {
	return true
}

func (s *serialPort) EchoMode(state bool) {
	s.echoMode = state
}

func (s *serialPort) Write(p []byte) (int, error) {
	if !s.echoMode {
		return s.Port.Write(p)
	} else {
		resp := make([]byte, 1)
		for i := 0; i < len(p); i++ {
			s.Port.Write(p[i : i+1])
			_, err := s.Port.Read(resp)
			if err != nil {
				return 0, err
			}
		}
	}
	return len(p), nil
}

type networkPort struct {
	conn     net.Conn
	echoMode bool
	address  string
	timeout  time.Duration
}

func NetworkPort(address string, timeout time.Duration) (*networkPort, error) {
	conn, err := net.DialTimeout("tcp", address, time.Second*5)
	if err != nil {
		return nil, err
	}

	return &networkPort{conn: conn,
		address: address, timeout: timeout}, nil
}

func (np *networkPort) String() string {
	return np.address
}

func (np *networkPort) SetBaud(baud int) error {
	return nil
}

func (np *networkPort) CanChangeBaud() bool {
	return false
}

func (np *networkPort) Read(p []byte) (int, error) {
	if np.timeout > 0 {
		np.conn.SetReadDeadline(time.Now().Add(np.timeout))
	}
	return np.conn.Read(p)
}

func (np *networkPort) EchoMode(state bool) {
	np.echoMode = state
}

func (np *networkPort) Write(p []byte) (int, error) {
	if !np.echoMode {
		return np.conn.Write(p)
	} else {
		resp := make([]byte, 1)
		for i := 0; i < len(p); i++ {
			np.conn.Write(p[i : i+1])
			_, err := np.conn.Read(resp)
			if err != nil {
				return 0, err
			}
		}
	}
	return len(p), nil
}

func (np *networkPort) Close() error {
	return np.conn.Close()
}
