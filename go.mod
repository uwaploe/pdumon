module bitbucket.org/uwaploe/pdumon

go 1.18

require (
	bitbucket.org/mfkenney/go-nmea v1.6.1
	bitbucket.org/uwaploe/pducom v0.9.16
	github.com/gomodule/redigo v1.8.4
	go.bug.st/serial v1.5.0
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	golang.org/x/sys v0.1.0 // indirect
)
