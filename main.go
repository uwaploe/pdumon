// Pdumon reads the PDU/PFU NMEA messages from a serial or TCP port and
// publishes "decoded" versions to a Redis server in JSON format.
package main

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/mfkenney/go-nmea"
	"bitbucket.org/uwaploe/pducom"
	"bitbucket.org/uwaploe/pdumon/internal/port"
	"github.com/gomodule/redigo/redis"
	"golang.org/x/exp/slog"
)

const Usage = `Usage: pdumon [options] addr [name]

Read NMEA messages from addr which can be either a serial device or HOST:PORT.
Each message is converted to JSON and published using Redis Pub/Sub channels.
If name is not supplied, it defaults to pdu. The channels are named "data.<name>.TAG"
where TAG is the NMEA sentence tag. Also, the raw NMEA sentences are published
to "nmea.<name>".

`

// PDU command terminator
const EOL = "\r"

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	msgTimeout time.Duration
	serialBaud int    = 115200
	rdAddr     string = "localhost:6379"
	msgQueue   int    = 10
	logCmds    bool
	ignoreCsum bool
	debugMode  bool
)

type dataMessage struct {
	T    time.Time   `json:"time"`
	Src  string      `json:"source"`
	Data interface{} `json:"data"`
	Raw  string      `json:"-"`
}

func publishMessage(conn redis.Conn, msg dataMessage, name string) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	_, err = conn.Do("PUBLISH", "data."+name+"."+msg.Src, b)
	if err != nil {
		return err
	}

	_, err = conn.Do("PUBLISH", "nmea."+name, []byte(msg.Raw))
	return err
}

// Pass all received Redis pub-sub messages to a Go channel.
func pubsubReader(psc redis.PubSubConn) <-chan redis.Message {
	c := make(chan redis.Message, 4)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				slog.Error("Redis error", msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					slog.Info("Pubsub channel closed",
						slog.String("channel", msg.Channel))
					return
				} else {
					slog.Info("Pubsub channel open",
						slog.String("channel", msg.Channel))
				}
			case redis.Message:
				select {
				case c <- msg:
				default:
					slog.Warn("Output queue full; message dropped")
				}
			}
		}
	}()

	return c
}

func pduMonitor(ctx context.Context, rdr io.Reader, qlen int) <-chan dataMessage {
	ch := make(chan dataMessage, qlen)
	rbuf := bufio.NewReader(rdr)
	csumErr, parseErr := 0, 0

	go func() {
		defer close(ch)
		for {
			s, err := nmea.ReadSentence(rbuf)
			switch {
			case errors.Is(err, os.ErrDeadlineExceeded):
				slog.Error("timeout", err)
				return
			case errors.Is(err, nmea.ErrChecksum):
				if !ignoreCsum {
					slog.Debug("checksum error", slog.String("data", s.String()))
					csumErr += 1
					continue
				}
			case err != nil:
				slog.Error("", err)
				continue
			}

			if s.IsEmpty() {
				continue
			}

			s = pducom.RefmtSentence(s)
			obj, err := pducom.DecodeSentence(s)
			if err != nil {
				slog.Debug("parsing error", slog.String("data", s.String()))
				parseErr += 1
				continue
			}

			m := dataMessage{
				T:    time.Now(),
				Src:  s.Id,
				Data: obj,
				Raw:  s.String(),
			}

			select {
			case ch <- m:
			case <-ctx.Done():
				slog.Info("error count",
					slog.Int("checksum", csumErr),
					slog.Int("parse", parseErr))
				return
			default:
				slog.Warn("Input queue full; message dropped",
					slog.String("id", s.Id))
			}
		}
	}()

	return ch
}

func sendPduCmd(wtr io.Writer, s nmea.Sentence) error {
	b, err := s.MarshalText()
	if err != nil {
		return err
	}
	wtr.Write(b)
	_, err = wtr.Write([]byte(EOL))
	if logCmds {
		slog.Info("Sent command",
			slog.String("text", string(b)))
	}
	return err
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&msgTimeout, "timeout",
		lookupEnvOrDuration("PDU_TIMEOUT", msgTimeout),
		"message timeout")
	flag.IntVar(&serialBaud, "baud",
		lookupEnvOrInt("PDU_BAUD", serialBaud),
		"serial baud rate")
	flag.IntVar(&msgQueue, "qlen",
		lookupEnvOrInt("PDU_QLEN", msgQueue),
		"message queue length")
	flag.StringVar(&rdAddr, "rdaddr",
		lookupEnvOrString("PDU_REDIS", rdAddr),
		"Redis host:port for message publishing")
	flag.BoolVar(&logCmds, "log-cmds",
		envVarExists("PDU_LOGCMDS"),
		"Log all sent commands")
	flag.BoolVar(&ignoreCsum, "ignore-csum",
		envVarExists("PDU_IGNORE_CSUM"),
		"Ignore NMEA checksum errors")
	flag.BoolVar(&debugMode, "debug",
		envVarExists("PDU_DEBUG"),
		"Enable debugging output")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func initLogger(verbose bool) *slog.Logger {
	opts := slog.HandlerOptions{}
	if verbose {
		opts.Level = slog.LevelDebug
	}
	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		opts.ReplaceAttr = func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		}
	}

	return slog.New(opts.NewTextHandler(os.Stderr))
}

func abort(msg string, args ...any) {
	slog.Error(msg, args...)
	os.Exit(1)
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	slog.SetDefault(initLogger(debugMode))

	devName := "pdu"
	if len(args) > 1 {
		devName = args[1]
	}

	var (
		p   port.Port
		err error
	)

	if strings.Contains(args[0], ":") {
		p, err = port.NetworkPort(args[0], msgTimeout)
		slog.Info("Connected", slog.String("addr", args[0]))
	} else {
		p, err = port.SerialPort(args[0], serialBaud, msgTimeout)
	}

	if err != nil {
		abort("Cannot connect to data source", err,
			slog.String("addr", args[0]))
	}
	defer p.Close()

	var (
		rdconn redis.Conn
		psc    redis.PubSubConn
		cmdch  <-chan redis.Message
	)

	rdconn, err = redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("Cannot connect to Redis server", err,
			slog.String("addr", rdAddr))
	}
	// Open a second connection to read commands
	c, _ := redis.Dial("tcp", rdAddr)
	psc = redis.PubSubConn{Conn: c}
	cmdch = pubsubReader(psc)
	psc.Subscribe(devName + ".command")

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	slog.Info("inVADER PDU/PFU Monitor", slog.String("version", Version))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ch := pduMonitor(ctx, p, msgQueue)
	for {
		select {
		case sig := <-sigs:
			slog.Info("Interrupted", slog.String("signal", sig.String()))
			if rdconn != nil {
				psc.Unsubscribe()
			}
			cancel()
			return
		case msg, ok := <-ch:
			if !ok {
				slog.Info("Data stream ended", slog.String("src", devName))
				return
			}
			if err = publishMessage(rdconn, msg, devName); err != nil {
				slog.Error("Redis PUB", err)
			}
		case cmd, ok := <-cmdch:
			if !ok {
				cmdch = nil
				slog.Warn("Command channel closed")
				cancel()
				return
			} else {
				s := nmea.Sentence{}
				if cmd.Data[0] != '$' {
					// Construct the Sentence manually
					fields := strings.Split(string(cmd.Data), ",")
					s.Id = fields[0]
					s.Fields = fields[1:]
				} else {
					s.UnmarshalText(cmd.Data)
				}
				err = sendPduCmd(p, s)
				if err != nil {
					slog.Error("Command send failed", err)
				}
			}
		}
	}
}
