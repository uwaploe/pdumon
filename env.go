package main

import (
	"os"
	"strconv"
	"time"

	"golang.org/x/exp/slog"
)

func envVarExists(key string) bool {
	_, ok := os.LookupEnv(key)
	return ok
}

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func lookupEnvOrInt(key string, defaultVal int) int {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.Atoi(val)
		if err != nil {
			slog.Error("LookupEnvOrInt", err,
				slog.String("key", key))
			os.Exit(2)
		}
		return v
	}
	return defaultVal
}

func lookupEnvOrFloat(key string, defaultVal float64) float64 {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.ParseFloat(val, 64)
		if err != nil {
			slog.Error("LookupEnvOrFloat", err,
				slog.String("key", key))
			os.Exit(2)
		}
		return v
	}
	return defaultVal
}

func lookupEnvOrDuration(key string, defaultVal time.Duration) time.Duration {
	if val, ok := os.LookupEnv(key); ok {
		v, err := time.ParseDuration(val)
		if err != nil {
			slog.Error("LookupEnvOrDuration", err,
				slog.String("key", key))
			os.Exit(2)
		}
		return v
	}
	return defaultVal
}
