# PDU/PFU Monitor

Pdumon reads the PDU or PFU NMEA messages from a serial port or TCP port and publishes "decoded" versions to [Redis](https://redis.io) Pub-Sub channels JSON format. It also monitors a channel for commands which will be forwarded to the PDU/PFU.

## Usage

This is for background information only, this program normally runs as Systemd service. Installing the Debian Linux package installs the Systemd unit files `/lib/systemd/system/pdumon.service` and `/lib/systemd/system/pfumon.service`.

``` shellsession
$ pdumon --help
Usage: pdumon [options] addr [name]

Read NMEA messages from addr which can be either a serial device or HOST:PORT.
Each message is converted to JSON and published using Redis Pub/Sub channels.
If name is not supplied, it defaults to pdu. The channels are named "data.<name>.TAG"
where TAG is the NMEA sentence tag. Also, the raw NMEA sentences are published
to "nmea.<name>".

  -baud int
        serial baud rate (default 115200)
  -ignore-csum
        Ignore NMEA checksum errors
  -log-cmds
        Log all sent commands
  -qlen int
        message queue length (default 10)
  -rdaddr string
        Redis host:port for message publishing (default "localhost:6379")
  -timeout duration
        message timeout
  -version
        Show program version information and exit
```

### Service Control

Starting.

``` shellsession
$ systemctl start pdumon.service
$ systemctl start pfumon.service
```

Stopping.

``` shellsession
$ systemctl stop pdumon.service
$ systemctl stop pfumon.service
```

## Redis Pub-Sub

### Data

Each decoded message is published to the channel *data.<name>.TAG*, where *<name>* is "pdu" or "pfu" and *TAG* is the NMEA sentence tag. The stream of messages can be viewed using the `redis-cli` command:

``` shellsession
$ redis-cli --raw psubscribe 'data.pdu.*'
psubscribe
data.pdu.*
1
pmessage
data.pdu.*
data.pdu.ADC4R
{"time":"2023-04-05T13:48:21.855787304-07:00","source":"ADC4R","data":{"scan":[67,125,13508,70,315,314,359
,105]}}
pmessage
data.pdu.*
data.pdu.ADC5R
{"time":"2023-04-05T13:48:21.860368723-07:00","source":"ADC5R","data":{"scan":[58639,34653,25801,53270,203
7,2013,1940,1949]}}
pmessage
data.pdu.*
data.pdu.HCB.ADC
{"time":"2023-04-05T13:48:23.184104232-07:00","source":"HCB.ADC","data":{"scan":[43,0,0,0,660,662,663,662]
}}
pmessage
data.pdu.*
data.pdu.BSTAT
{"time":"2023-04-05T13:48:23.728000662-07:00","source":"BSTAT","data":{"boot":36,"uptime":0}}
pmessage
data.pdu.*
data.pdu.POWER
{"time":"2023-04-05T13:48:23.738849019-07:00","source":"POWER","data":{"BEXP":false,"CAM1":false,"CAM2":fa
lse,"CCD":false,"EXPAN":false,"FOCUS":false,"HCB":true,"HCBMODE":false,"LASER12":false,"LASER28":false,"LA
SER5":false,"LED1":false,"LED2":false,"TB":false,"UV":false}}
```
Type `ctrl-c` to exit.

Note that the *$HCB* sentences (from the Heater Control Board) are given the following "pseudo" tags to make them easier to filter.

* HSB.ADC
* HSB.ALLTEMP
* HSB.SETTING

The raw NMEA sentences are published to *nmea.<name>*.

### Commands

NMEA commands can be sent to the PDU/PFU via the *<name>.command* channel, there is no need to specify the leading `$` or trailing checksum:


``` shellsession
$ redis-cli publish pdu.command 'PWREN,CCD,1'
```

## Message Formats

Each JSON-encoded message has the following general format:

``` json
{
  "time": <UTC timestamp in RFC-3339 format>,
  "source": <NMEA tag>,
  "data": <Parsed data object>
}
```
